import { DEFAULT_VALUES } from '../common/constants';

export class Globals {

  static fuel: number;  // percentage 0-100
  static time: number;  // in seconds
  static map: string;   // map ID

  static helpTextShown: boolean;

  static reset(): void {
    Globals.fuel = DEFAULT_VALUES.MAX_FUEL;
    Globals.time = DEFAULT_VALUES.TIME;
    Globals.map = DEFAULT_VALUES.MAP;
  }

  static decreaseFuel(amount: number): void {
    Globals.fuel -= amount;
    if (Globals.fuel < 0) {
      Globals.fuel = 0;
    }
  }

  static maximizeFuel(): void {
    Globals.fuel = DEFAULT_VALUES.MAX_FUEL;
  }

  static increaseTime(amount?: number): void {
    Globals.time += amount || 1;
  }
}