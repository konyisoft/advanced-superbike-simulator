export class Utils {

  // Returns a random integer between min (inclusive) and max (inclusive)
  static randomInt(min: number, max: number): number {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  // Returns time string in hh:mm format
  static formatTime(timeInSeconds: number): string {
    let minutes: number = Math.floor(timeInSeconds / 60);
    let seconds: number = timeInSeconds % 60;
    return `${minutes.toString().padStart(2,'0')}:${seconds.toString().padStart(2,'0')}`;
  }


}