export const SCREEN = {
  // Virtual grid of scene
  GRID: {
    COLS: 18,
    ROWS: 10
  },
  // Pixel size of a cell in the grid. Width and height are equal.
  CELL: {
    SIZE: 32,
    HALFSIZE: 16
  },
  // Calculated canvas size
  get CANVAS() {
    return {
      WIDTH: this.GRID.COLS * this.CELL.SIZE,
      HEIGHT: this.GRID.ROWS * this.CELL.SIZE
    }
  },
}

// Camera settings
export const CAMERA = {
  MAIN: {
    GRID_WIDTH: 18,
    GRID_HEIGHT: 9,
    get PIXEL_WIDTH() {
      return CAMERA.MAIN.GRID_WIDTH * SCREEN.CELL.SIZE;
    },
    get PIXEL_HEIGHT() {
      return CAMERA.MAIN.GRID_HEIGHT * SCREEN.CELL.SIZE;
    }
  },
  PANEL: {
    GRID_WIDTH: 18,
    GRID_HEIGHT: 1,
    get PIXEL_WIDTH() {
      return CAMERA.PANEL.GRID_WIDTH * SCREEN.CELL.SIZE;
    },
    get PIXEL_HEIGHT() {
      return CAMERA.PANEL.GRID_HEIGHT * SCREEN.CELL.SIZE;
    }
  },
}

// Assets directory
export const DIRECTORY = {
  FONTS: 'assets/fonts/',
  MAPS: 'assets/maps/',
  JS: 'assets/js/',
  IMAGES: {
    PARTICLES: 'assets/images/particles/',
    SPRITES: 'assets/images/sprites/',
    TILESETS: 'assets/images/tilesets/',
  }
}

// File extensions
export const EXTENSION = {
  JSON: '.json',
  PNG: '.png',
}

// Scene IDs
export const SCENE = {
  PRELOAD: 'preload-scene',
  MAIN: 'main-scene',
}

// Sprite IDs (=file names)
export const SPRITE = {
  PLAYER: 'player',
  FUEL: 'fuel',
}

// Particle image IDs (=file names)
export const PARTICLE = {
  EXPLOSION: 'explosion',
}

export let COLLIDER = {
  'player': { width: 28, height: 8 },
  'fuel': { width: 14, height: 16 },
}

export const DEPTH = {
  TILEMAP: 0,
  PLAYER: 10,
  ITEM: 20,
  PARTICLES: 30,
  UI: 40,
  UI_TEXT: 50,
}

export const MAP = {
  MAP_1: 'map-1',
}

export const MAP_LAYER = {
  GROUND: 'Ground',
  ADDITIONAL: 'Additional',
  OBJECTS: 'Objects',
  SPAWN: 'Spawn',
}

export const TILE_DATA = {
  IMAGE: 'superbike.extruded',
  TILESET: 'superbike',
  TILE: {
    HEIGHT: 32,
    WIDTH: 32,
  }
}

export const TILE_PROPERTY = {
  ANGLE: 'angle',
  DEADLY: 'deadly',
  FINISH_LINE: 'finish_line',
  SPRITE: 'sprite',
  IS_PLAYER: 'isPlayer',
  IS_ITEM: 'isItem',
}

export const BODY_LABEL = {
  DEADLY: 'deadly',
  FINISH_LINE: 'finish_line',
}

export const FONT = {
  GENERAL: {
    color: '#eee',
    fontSize: '1rem',
    fontFamily: 'mikropix',
    shadow: {
      offsetX: 1,
      offsetY: 1,
      color: '#000',
    }
  },
  LARGE: {
    color: '#eee',
    fontSize: '3rem',
    fontFamily: 'mikropix',
    shadow: {
      offsetX: 3,
      offsetY: 3,
      color: '#000',
    }
  },
}

export const DEFAULT_VALUES = {
  MAX_FUEL: 100,
  TIME: 0,
  MAP: MAP.MAP_1,
}

export const TEXT = {
  FUEL: 'FUEL:',
  TIME: 'TIME:',
  TITLE: 'Advanced Superbike Simulator',
  GAME_OVER: "GAME OVER",
  LEVEL_COMPLETED: "LEVEL COMPLETED",
  HELP: 'PRESS SPACE OR ENTER / LEFT MOUSE BUTTON / TOUCH THE SCREEN\nTO ACCELERATE THE SUPERBIKE',
}

