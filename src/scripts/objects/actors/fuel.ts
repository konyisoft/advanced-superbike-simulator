import { game } from '../../game';
import { Item } from './item';
import { Utils } from '../../common/utils';
import { SPRITE } from '../../common/constants';

export class Fuel extends Item {

  private tween: Phaser.Tweens.Tween;

  constructor(scene: Phaser.Scene, x: number, y: number) {
    super(scene, x, y, SPRITE.FUEL);
    this.tween = scene.tweens.add({
      targets: this,
      y: this.y - 7,
      delay: Utils.randomInt(500, 1000),
      duration: 150,
      ease: 'Linear',
      repeat: -1,
      repeatDelay: 1500,
      yoyo: true
    });
  }

  destroy(fromScene?: boolean): void {
    this.currentScene.tweens.remove(this.tween);
    super.destroy(fromScene);
  }

  protected onCollideWithPlayer(): void {
    super.onCollideWithPlayer();
    game.playerManager.maximizeFuel();
  }
}