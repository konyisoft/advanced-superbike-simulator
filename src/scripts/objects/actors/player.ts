import { game } from '../../game';
import { Actor } from './actor';
import { BODY_LABEL, SPRITE } from '../../common/constants';

export enum PlayerState {
  Standing,
  Rotating,
  Moving,
  Terminated,
}

export class Player extends Actor {

  private rotationSpeed: number = 0.25;
  private rotationFuelConsumption: number = 1;
  private moveSpeed: number = 0.11;
  private moveFuelConsumption: number = 6.5;
  private playerState: PlayerState = PlayerState.Standing; // by default

  constructor(scene: Phaser.Scene, x: number, y: number, angle: number) {
    super(scene, x, y, SPRITE.PLAYER);
    this.setAngle(angle);
    this.setupTileColllions();
  }

  get PlayerState(): PlayerState {
    return this.playerState;
  }

  get IsRotating(): boolean {
    return this.playerState == PlayerState.Rotating;
  }

  get IsTerminated(): boolean {
    return this.playerState == PlayerState.Terminated;
  }

  stand(): void {
    this.setPlayerState(PlayerState.Standing);
  }

  rotate(): void {
    this.setPlayerState(PlayerState.Rotating);
  }

  move(): void {
    this.setPlayerState(PlayerState.Moving);
  }

  terminate(): void {
    this.setPlayerState(PlayerState.Terminated);
  }

  hide(delay?: number): void {
    if (delay) {
      setTimeout(() => { this.setVisible(false); }, delay);
    } else {
      this.setVisible(false);
    }
  }

  show(): void {
    this.setVisible(true);
  }

  update(time: number, delta: number): void {
    switch (this.playerState) {
      case PlayerState.Rotating:
        this.angle += delta * this.rotationSpeed;
        game.playerManager.decreaseFuel(this.rotationFuelConsumption * (delta / 1000));
        break;
      case PlayerState.Moving:
        let deltaX: number = Math.cos(this.rotation) * this.moveSpeed * delta;
        let deltaY: number = Math.sin(this.rotation) * this.moveSpeed * delta;
        this.x += deltaX;
        this.y += deltaY;
        game.playerManager.decreaseFuel(this.moveFuelConsumption * (delta / 1000));
        break;
    }
  }

  private setPlayerState(playerState: PlayerState): boolean {
    if (this.playerState != playerState) {
      this.playerState = playerState;
      return true;
    }
    return false;
  }

  private setupTileColllions(): void {
    // Setup player <-> tile collisions
    this.currentScene.matter.world.on('collisionstart', (event: any, bodyA: any, bodyB: any) => {
      if (this.checkCollision(bodyA, bodyB, BODY_LABEL.DEADLY)) {
        this.onCollideDeadlyTile();
      } else if (this.checkCollision(bodyA, bodyB, BODY_LABEL.FINISH_LINE)) {
        this.onCollideFinishLine();
      }
    });
  }

  private isBodyLabel(body: any, label: string): boolean {
    // NOTE: Why parenting some of the bodies? Maybe the collider created with Tiled is too complex?
    return body.label == label || (body.parent && body.parent.label == label);
  }

  private checkCollision(bodyA: any, bodyB: any, label: string): boolean {
    return (bodyA == this.body && this.isBodyLabel(bodyB, label)) || (bodyB == this.body && this.isBodyLabel(bodyA, label));
  }

  private onCollideDeadlyTile(): void {
    game.playerManager.playerCollided();
  }

  private onCollideFinishLine(): void {
    game.playerManager.levelCompleted();
  }
}