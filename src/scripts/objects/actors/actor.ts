import { COLLIDER, SCREEN } from '../../common/constants';

export class Actor extends Phaser.Physics.Matter.Sprite {

  protected currentScene: Phaser.Scene;

  constructor(scene: Phaser.Scene, x: number, y: number, spriteId: string) {
    super(scene.matter.world, x, y, spriteId);
    scene.add.existing(this);
    this.currentScene = scene;
    this.setDisplayOrigin(SCREEN.CELL.HALFSIZE, SCREEN.CELL.HALFSIZE); // rotates around the center point
    if (typeof COLLIDER[spriteId] !== 'undefined') {
      this.setRectangle(COLLIDER[spriteId].width, COLLIDER[spriteId].height);
    } else {
      this.setRectangle(SCREEN.CELL.SIZE, SCREEN.CELL.SIZE);
    }
    this.setPosition(x, y);
    this.setSensor(true);
  }
}