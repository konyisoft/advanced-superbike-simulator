import { game } from '../../game';
import { Actor } from './actor';

export class Item extends Actor {

  constructor(scene: Phaser.Scene, x: number, y: number, spriteId: string) {
    super(scene, x, y, spriteId);
    this.setOnCollideWith(game.playerManager.Player, () => this.onCollideWithPlayer());
  }

  protected onCollideWithPlayer(): void {
    game.itemManager.removeItem(this);
  }
}