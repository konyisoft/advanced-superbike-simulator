export class FuelBar {

  private rect: Phaser.Geom.Rectangle;
  private graphics: Phaser.GameObjects.Graphics;
  private percentage: number;  // between 0 and 100
  private backgroundColor: number = 0x888888;
  private normalColorTop: number = 0xffff00;
  private normalColorBottom: number = 0xffeb04;
  private warningColorTop: number = 0xff6633;
  private warningColorBottom: number = 0xdd5522;
  private warningPercentage: number = 25;

  constructor(graphics: Phaser.GameObjects.Graphics, x: number, y: number, width: number, height: number, percentage: number) {
    this.rect = new Phaser.Geom.Rectangle(x, y, width, height);
    this.graphics = graphics;
    this.percentage = percentage;
    this.draw();
  }

  setPercentage(percentage: number): void {
    this.percentage = percentage;
    this.draw();
  }

  draw(): void {
    this.graphics.fillStyle(this.backgroundColor, 1);
    this.graphics.fillRect(this.rect.x, this.rect.y, this.rect.width, this.rect.height);
    // Gradient colors
    let isWarning: boolean = this.percentage <= this.warningPercentage;
    let colorTop: number = isWarning ? this.warningColorTop : this.normalColorTop;
    let colorBottom: number = isWarning ? this.warningColorBottom : this.normalColorBottom;
    this.graphics.fillGradientStyle(colorTop, colorTop, colorBottom, colorBottom);
    this.graphics.fillRect(this.rect.x, this.rect.y, Math.floor(this.rect.width * (this.percentage / 100)), this.rect.height);
  }
}