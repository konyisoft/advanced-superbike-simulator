import 'phaser';
import * as Managers from './managers';
import { MainScene, PreloadScene } from './scenes';
import { SCREEN } from './common/constants';

const config: Phaser.Types.Core.GameConfig = {
  type: Phaser.AUTO,
  backgroundColor: '#121212',
  input: {
    keyboard: true,
  },
  scale: {
    parent: 'phaser-game',
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
    width: SCREEN.CANVAS.WIDTH,
    height: SCREEN.CANVAS.HEIGHT,
  },
  render: {
    pixelArt: true,
  },
  scene: [PreloadScene, MainScene],
  physics: {
    default: 'matter',
    matter: {
      gravity: false,
      debug: false,
    }
  }
}

export class Game extends Phaser.Game {

  cameraManager: Managers.CameraManager;
  inputManager: Managers.InputManager;
  itemManager: Managers.ItemManager;
  mapManager: Managers.MapManager;
  panelManager: Managers.PanelManager;
  particleManager: Managers.ParticleManager;
  playerManager: Managers.PlayerManager;
  textManager: Managers.TextManager;
  timeManager: Managers.TimeManager;

  constructor(config?: Phaser.Types.Core.GameConfig) {
    super(config);
    this.createMangers();
  }

  private createMangers(): void {
    this.cameraManager = new Managers.CameraManager();
    this.inputManager = new Managers.InputManager();
    this.itemManager = new Managers.ItemManager();
    this.mapManager = new Managers.MapManager();
    this.panelManager = new Managers.PanelManager();
    this.particleManager = new Managers.ParticleManager();
    this.playerManager = new Managers.PlayerManager();
    this.textManager = new Managers.TextManager();
    this.timeManager = new Managers.TimeManager();
  }
}

export let game: Game;

window.addEventListener('load', () => {
  game = new Game(config)
})
