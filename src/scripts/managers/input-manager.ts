import { Manager } from './manager';

export class InputManager extends Manager {

  private keys: any = {};
  private isWaitingForUp: boolean = false;

  init(scene: Phaser.Scene): void {
    super.init(scene);
    this.initKeys();
  }

  update(delta: number, time: number): void {
    // All the keys and the pointer are up?
    if (this.isWaitingForUp && this.keys.accelerate.isUp && this.keys.accelerateAlt.isUp && !this.IsPointerDown) {
      this.isWaitingForUp = false;
    }
  }

  reset(): void {
    // Waits for all ths keys and the pointer are up.
    // IsDown returns with false until then.
    this.isWaitingForUp = true;
  }

  cleanup(): void {
    this.currentScene.input.keyboard.removeAllKeys();
    super.cleanup();
  }

  get IsDown(): boolean {
    // Is any key or pointer down?
    return !this.isWaitingForUp && (this.keys.accelerate.isDown || this.keys.accelerateAlt.isDown || this.IsPointerDown);
  }

  private get IsPointerDown(): boolean {
    return this.currentScene.input.activePointer && this.currentScene.input.activePointer.isDown;
  }

  private initKeys(): void {
    this.keys = this.currentScene.input.keyboard.addKeys({
      'accelerate': Phaser.Input.Keyboard.KeyCodes.SPACE,
      'accelerateAlt': Phaser.Input.Keyboard.KeyCodes.ENTER,
    });
  }
}