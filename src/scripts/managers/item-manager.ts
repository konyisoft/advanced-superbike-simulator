import { Manager } from './manager';
import { Item } from '../objects/actors';
import { Fuel } from '../objects/actors';
import { DEPTH, MAP_LAYER, SCREEN, SPRITE, TILE_PROPERTY } from '../common/constants';

export class ItemManager extends Manager {

  private items: Item[] = [];

  init(scene: Phaser.Scene): void {
    super.init(scene);
    this.group.setDepth(DEPTH.ITEM);
  }

  createItems(map: Phaser.Tilemaps.Tilemap ): void {
    // Iterate through spawn layer tiles and create items on spawn points
    map.forEachTile((tile: Phaser.Tilemaps.Tile) => {
      if (tile.properties[TILE_PROPERTY.IS_ITEM]) {
        let x: number = (tile.x * SCREEN.CELL.SIZE) + SCREEN.CELL.HALFSIZE;
        let y: number = (tile.y * SCREEN.CELL.SIZE) + SCREEN.CELL.HALFSIZE;
        this.addItem(x, y, tile.properties[TILE_PROPERTY.SPRITE]);
      }
    }, this, 0, 0, map.width, map.height, {}, MAP_LAYER.SPAWN);
  }

  addItem(x: number, y: number, spriteId: string): void {
    let item: Item = null;
    // Create item based on its sprite
    switch (spriteId) {
      case SPRITE.FUEL:
        item = new Fuel(this.currentScene, x, y);
        break;
    }
    // Push to array
    if (item != null) {
      this.items.push(item);
      this.group.add(item);
    }
  }

  removeItem(item: Item): void {
    let index: number = this.items.indexOf(item);
    if (index > -1) {
      let item: Item = this.items[index];
      this.currentScene.matter.world.remove(item);
      this.group.remove(item);
      this.items.splice(index, 1);
      item.destroy(true);
    }
  }

  cleanup(): void {
    this.removeAll();
    super.cleanup();
  }

  get Items(): Item[] {
    return this.items;
  }

  private removeAll(): void {
    for (let i = 0; i < this.items.length; i++) {
      let item: Item = this.items[i];
      this.currentScene.matter.world.remove(item);
      this.group.remove(item);
      item.destroy(true);
    }
    this.items = [];
  }

}