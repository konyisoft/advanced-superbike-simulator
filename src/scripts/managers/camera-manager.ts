import { game } from '../game';
import { Manager } from './manager';
import { CAMERA } from '../common/constants';

export class CameraManager extends Manager {

  private mainCamera: Phaser.Cameras.Scene2D.Camera;
  private panelCamera: Phaser.Cameras.Scene2D.Camera;

  setupCameras(): void {
    // Main camera (tilemap)
    this.mainCamera = this.currentScene.cameras.main;
    this.mainCamera.setPosition(0, 0);
    this.mainCamera.setSize(CAMERA.MAIN.PIXEL_WIDTH, CAMERA.MAIN.PIXEL_HEIGHT);
    this.mainCamera.ignore(game.panelManager.Group);
    // Scrolling
    this.mainCamera.setBounds(0, 0, game.mapManager.CurrentMap.widthInPixels, game.mapManager.CurrentMap.heightInPixels);
    this.mainCamera.startFollow(game.playerManager.Player);
    // Panel camera
    this.panelCamera = this.currentScene.cameras.add();
    this.panelCamera.setPosition(0, CAMERA.MAIN.PIXEL_HEIGHT);
    this.panelCamera.setSize(CAMERA.PANEL.PIXEL_WIDTH, CAMERA.PANEL.PIXEL_HEIGHT);
    this.panelCamera.ignore(game.itemManager.Group);
    this.panelCamera.ignore(game.mapManager.Group);
    this.panelCamera.ignore(game.particleManager.Group);
    this.panelCamera.ignore(game.playerManager.Group);
    this.panelCamera.ignore(game.textManager.Group);
  }

  fadeIn(callback?: Function): void {
    this.mainCamera.once('camerafadeincomplete', () => {
      if (callback) {
        callback();
      }
    });
    this.mainCamera.fadeFrom(1000, 18, 18, 18);
    this.panelCamera.fadeFrom(1000, 18, 18, 18);
  }

  fadeOut(callback?: Function): void {
    this.mainCamera.once('camerafadeoutcomplete', () => {
      if (callback) {
        callback();
      }
    });
    this.mainCamera.fade(1000, 18, 18, 18);
    this.panelCamera.fade(1000, 18, 18, 18);
  }

  cleanup(): void {
    this.currentScene.cameras.resetAll();
    this.mainCamera = null;
    this.panelCamera = null;
    super.cleanup();
  }

  get MainCamera(): Phaser.Cameras.Scene2D.Camera {
    return this.mainCamera;
  }

  get PanelCamera(): Phaser.Cameras.Scene2D.Camera {
    return this.panelCamera;
  }
}