import { Manager } from './manager';
import { BODY_LABEL, DEPTH, MAP_LAYER, TILE_DATA, TILE_PROPERTY } from '../common/constants';

export class MapManager extends Manager {

  private currentMap: Phaser.Tilemaps.Tilemap;
  private groundLayer: Phaser.Tilemaps.StaticTilemapLayer;
  private additionalLayer: Phaser.Tilemaps.StaticTilemapLayer;
  private objectsLayer: Phaser.Tilemaps.DynamicTilemapLayer;
  private spawnLayer: Phaser.Tilemaps.StaticTilemapLayer;

  init(scene: Phaser.Scene): void {
    super.init(scene);
    this.group.setDepth(DEPTH.TILEMAP);
  }

  createMap(key: string): void {
    // Map
    this.currentMap = this.currentScene.make.tilemap({ key: key });
    // Tiles
    // NOTE: Extruded tiles with 1px margin and 2px spacing
    // SEE: https://github.com/sporadic-labs/tile-extruder
    this.currentMap.addTilesetImage(TILE_DATA.TILESET, TILE_DATA.IMAGE, TILE_DATA.TILE.WIDTH, TILE_DATA.TILE.HEIGHT, 1, 2);
    // Layers
    this.groundLayer = this.currentMap.createStaticLayer(MAP_LAYER.GROUND, TILE_DATA.TILESET);
    this.additionalLayer = this.currentMap.createStaticLayer(MAP_LAYER.ADDITIONAL, TILE_DATA.TILESET);
    this.objectsLayer = this.currentMap.createDynamicLayer(MAP_LAYER.OBJECTS, TILE_DATA.TILESET);
    this.spawnLayer = this.currentMap.createStaticLayer(MAP_LAYER.SPAWN, '');  // no tileset added (invisible tiles)
    this.group.add(this.groundLayer);
    this.group.add(this.additionalLayer);
    this.group.add(this.objectsLayer);
    this.group.add(this.spawnLayer);
    this.setupTileCollisions();
  }

  cleanup(): void {
    this.currentMap.destroy();
    this.currentMap = null;
    this.groundLayer = null;
    this.additionalLayer = null;
    this.objectsLayer = null;
    this.spawnLayer = null;
    super.cleanup();
  }

  get CurrentMap(): Phaser.Tilemaps.Tilemap {
    return this.currentMap;
  }

  get ObjectsLayer(): Phaser.Tilemaps.DynamicTilemapLayer {
    return this.objectsLayer;
  }

  private setupTileCollisions(): void {
    this.objectsLayer.setCollisionFromCollisionGroup();
    this.currentScene.matter.world.convertTilemapLayer(this.objectsLayer);
    this.currentScene.matter.world.setBounds(0, 0, this.currentMap.widthInPixels, this.currentMap.heightInPixels);
    this.objectsLayer.forEachTile((tile: any) => {
      if (tile.physics.matterBody) {
        tile.physics.matterBody.setSensor(true);
        if (tile.properties[TILE_PROPERTY.DEADLY]) {
          tile.physics.matterBody.body.label = BODY_LABEL.DEADLY;
        } else if (tile.properties[TILE_PROPERTY.FINISH_LINE]) {
          tile.physics.matterBody.body.label = BODY_LABEL.FINISH_LINE;
        }
      }
    });
  }

}