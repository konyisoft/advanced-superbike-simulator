import { Manager } from './manager';
import { DEPTH, PARTICLE } from '../common/constants';

export class ParticleManager extends Manager {

  private explosionParticle: Phaser.GameObjects.Particles.ParticleEmitterManager;
  private explosionEmitter: Phaser.GameObjects.Particles.ParticleEmitter;

  init(scene: Phaser.Scene): void {
    super.init(scene);
    this.group.setDepth(DEPTH.PARTICLES);
  }

  createParticles(): void {
    this.createExplosion();
  }

  cleanup(): void {
    this.destroyExplosion();
    super.cleanup();
  }

  get ExplosionEmitter(): Phaser.GameObjects.Particles.ParticleEmitter {
    return this.explosionEmitter;
  }

  private createExplosion(): void {
    this.explosionParticle = this.currentScene.add.particles(PARTICLE.EXPLOSION);
    this.explosionEmitter = this.explosionParticle.createEmitter({
      active: false,
      lifespan: 1600,
      speed: {
        min: -50,
        max: 50
      },
      scale: {
        start: 0.25,
        end: 0.35
      },
      alpha: {
        start: 1,
        end: 0
      },
      tint: [ 0xff0000, 0xff6633, 0xffff00, 0xffffff ],
    });
    this.group.add(this.explosionParticle);
  }

  private destroyExplosion(): void {
    this.explosionParticle.removeEmitter(this.explosionEmitter);
    this.group.remove(this.explosionParticle);
    this.explosionParticle.destroy(true);
  }
}