import { Manager } from './manager';
import { Globals } from '../common/globals';
import { Utils } from '../common/utils';
import { FuelBar } from '../objects/ui';
import { CAMERA, DEPTH, FONT, TEXT } from '../common/constants';

export class PanelManager extends Manager {

  private fuelBar: FuelBar;
  private timeText: Phaser.GameObjects.Text;
  private graphics: Phaser.GameObjects.Graphics;

  private backgroundColor: number = 0x707077;
  private borderColor: number = 0xffffff;
  private borderWidth: number = 2;
  private shadowWidth: number = 2;

  init(scene: Phaser.Scene): void {
    super.init(scene);
    this.graphics = new Phaser.GameObjects.Graphics(scene);
    scene.add.existing(this.graphics);
    this.group.add(this.graphics);
    this.group.setDepth(DEPTH.UI);
  }

  createPanel(): void {
    this.createTexts();
    this.createFuelBar();
  }

  redraw(): void {
    this.graphics.clear();
    // Border
    this.graphics.fillStyle(this.borderColor, 1);
    this.graphics.fillRect(0, 0, CAMERA.PANEL.PIXEL_WIDTH, CAMERA.PANEL.PIXEL_HEIGHT);
    // Background
    this.graphics.fillStyle(this.backgroundColor, 1);
    //this.graphics.fillGradientStyle(0x77777d, 0x77777d, 0x66666d, 0x66666d);
    this.graphics.fillRect(
      this.borderWidth,
      this.borderWidth,
      CAMERA.PANEL.PIXEL_WIDTH - (this.borderWidth * 2),
      CAMERA.PANEL.PIXEL_HEIGHT - (this.borderWidth * 2)
    );
    // Inner shadow
    this.graphics.fillStyle(0, 0.175);
    this.graphics.fillRect(
      this.borderWidth,
      this.borderWidth,
      CAMERA.PANEL.PIXEL_WIDTH - (this.borderWidth * 2),
      this.shadowWidth
    );
    this.graphics.fillRect(
      CAMERA.PANEL.PIXEL_WIDTH - (this.borderWidth * 2),
      this.borderWidth,
      this.shadowWidth,
      CAMERA.PANEL.PIXEL_HEIGHT - (this.borderWidth * 2)
    );
    // Fuel bar
    this.fuelBar.setPercentage(Globals.fuel);
    // Time text
    this.timeText.setText(Utils.formatTime(Globals.time));
  }

  cleanup(): void {
    this.graphics.clear();
    this.graphics.destroy(true);
    super.cleanup();
    // NOTE: All UI elements will be destroyed with the group
    this.fuelBar = null;
    this.timeText = null;
    this.graphics = null;
  }

  private createTexts(): void {
    this.createText(14, 4, TEXT.FUEL);
    this.createText(200, 4, TEXT.TIME);
    this.createText(CAMERA.PANEL.PIXEL_WIDTH - 14, 4, TEXT.TITLE).setOrigin(1, 0).setColor('#0ff');
    this.timeText = this.createText(254, 4, '00:00');
    this.timeText.setColor('#ffeb04');
  }

  private createText(x: number, y: number, t: string, style?: Phaser.Types.GameObjects.Text.TextStyle): Phaser.GameObjects.Text {
    style = style || FONT.GENERAL;
    let text: Phaser.GameObjects.Text = new Phaser.GameObjects.Text(this.currentScene, x, y, t, style);
    // NOTE: Shadow is not applied automatically. Possibly a bug?
    text.setShadow(style.shadow.offsetX, style.shadow.offsetY, style.shadow.color);
    text.setDepth(DEPTH.UI_TEXT);
    this.currentScene.add.existing(text);
    this.group.add(text);
    return text;
  }

  private createFuelBar(): void {
    this.fuelBar = new FuelBar(this.graphics, 68, 10, 112, 12, 100);
  }
}