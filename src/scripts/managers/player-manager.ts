import { game } from '../game';
import { Globals } from '../common/globals';
import { Manager } from './manager';
import { Player } from '../objects/actors';
import { DEPTH, MAP_LAYER, SCREEN, TILE_PROPERTY } from '../common/constants';

export class PlayerManager extends Manager {

  private player: Player = null;
  private playerDiedCallback: Function;
  private levelCompletedCallback: Function;

  init(scene: Phaser.Scene): void {
    super.init(scene);
    this.group.setDepth(DEPTH.PLAYER);
  }

  createPlayer(map: Phaser.Tilemaps.Tilemap, playerDiedCallback?: Function, levelCompletedCallback?: Function): void {
    // Iterate through spawn layer tiles and create player on spawn point
    map.forEachTile((tile: Phaser.Tilemaps.Tile) => {
      if (tile.properties[TILE_PROPERTY.IS_PLAYER] && !this.HasPlayer) {
        let x: number = (tile.x * SCREEN.CELL.SIZE) + SCREEN.CELL.HALFSIZE;
        let y: number = (tile.y * SCREEN.CELL.SIZE) + SCREEN.CELL.HALFSIZE;
        this.player = new Player(this.currentScene, x, y, tile.properties[TILE_PROPERTY.ANGLE]);
        this.group.add(this.player);
      }
    }, this, 0, 0, map.width, map.height, {}, MAP_LAYER.SPAWN);
    this.playerDiedCallback = playerDiedCallback;
    this.levelCompletedCallback = levelCompletedCallback;
  }

  decreaseFuel(amount: number): void {
    Globals.decreaseFuel(amount);
    game.panelManager.redraw();
    if (Globals.fuel <= 0) {
      this.outOfFuel();
    }
  }

  maximizeFuel(): void {
    Globals.maximizeFuel();
    game.panelManager.redraw();
    console.log('Maximize fuel: ', Globals.fuel);
  }

  playerCollided(): void {
    game.timeManager.pause();
    this.player.terminate();
    this.player.hide(250);
    this.explodePlayer();
    if (this.playerDiedCallback) {
      this.playerDiedCallback();
    }
    console.log('Game over');
  }

  outOfFuel(): void {
    game.timeManager.pause();
    this.player.terminate();
    this.player.hide(250);
    this.explodePlayer();
    if (this.playerDiedCallback) {
      this.playerDiedCallback();
    }
    console.log('Out of fuel');
  }

  levelCompleted(): void {
    game.timeManager.pause();
    this.player.terminate();
    if (this.levelCompletedCallback) {
      this.levelCompletedCallback();
    }
    console.log('Level completed');
  }

  cleanup(): void {
    this.destroyPlayer();
    super.cleanup();
  }

  get HasPlayer(): boolean {
    return this.player != null;
  }

  get Player(): Player {
    return this.player;
  }

  private explodePlayer(): void {
    game.particleManager.ExplosionEmitter.active = true;
    game.particleManager.ExplosionEmitter.explode(100, this.player.x, this.player.y);
  }

  private destroyPlayer(): void {
    if (this.player != null) {
      this.currentScene.matter.world.remove(this.player);
      this.group.remove(this.player);
      this.player.destroy(true);
      this.player = null;
      this.playerDiedCallback = null;
      this.levelCompletedCallback = null;
    }
  }
}