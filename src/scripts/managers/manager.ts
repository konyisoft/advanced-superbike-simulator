export class Manager {

  protected currentScene: Phaser.Scene;
  protected group: Phaser.GameObjects.Group;

  init(scene: Phaser.Scene): void {
    this.currentScene = scene;
    this.group = new Phaser.GameObjects.Group(scene);
  }

  cleanup(): void {
    this.group.destroy(true); // = true destroys all children of the group
    this.currentScene = null;
  }

  get Group(): Phaser.GameObjects.Group {
    return this.group;
  }
}