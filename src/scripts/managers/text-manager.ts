import { Manager } from './manager';
import { CAMERA, DEPTH, FONT, TEXT } from '../common/constants';

export class TextManager extends Manager {

  private gameOverText: Phaser.GameObjects.Text;
  private levelCompletedText: Phaser.GameObjects.Text;
  private helpText: Phaser.GameObjects.Text;
  private textTween: Phaser.Tweens.Tween;

  init(scene: Phaser.Scene): void {
    super.init(scene);
    this.group.setDepth(DEPTH.UI);
  }

  createTexts(): void {
    let centerX: number = CAMERA.MAIN.PIXEL_WIDTH / 2;
    let centerY: number = CAMERA.MAIN.PIXEL_HEIGHT / 2;
    this.gameOverText = this.createText(centerX, centerY, TEXT.GAME_OVER, FONT.LARGE).setColor('#f00');
    this.levelCompletedText = this.createText(centerX, centerY, TEXT.LEVEL_COMPLETED, FONT.LARGE).setColor('#bcf');
    this.helpText = this.createText(centerX, 20, TEXT.HELP).setColor('#fff').setAlign('center');

    // Hide all at the beginning
    this.setGameOverTextVisible(false);
    this.setLevelCompletedTextVisible(false);
    this.setHelpTextVisible(false);

    this.textTween = this.currentScene.tweens.add({
      targets: [this.gameOverText, this.levelCompletedText],
      scale: 1.15,
      duration: 150,
      ease: 'Cubic.easeOut',
      yoyo: true,
    });
    this.textTween.pause();
  }

  setGameOverTextVisible(visible: boolean): void {
    this.gameOverText.setVisible(visible);
    if (visible) {
      this.textTween.play();
    }
  }

  setLevelCompletedTextVisible(visible: boolean): void {
    this.levelCompletedText.setVisible(visible);
    if (visible) {
      this.textTween.play();
    }
  }

  setHelpTextVisible(visible: boolean): void {
    this.helpText.setVisible(visible);
  }

  cleanup(): void {
    this.currentScene.tweens.remove(this.textTween);
    // NOTE: All texts will be destroyed with the group
    super.cleanup();
  }

  get IsHelpVisible(): boolean {
    return this.helpText.visible;
  }

  private createText(x: number, y: number, t: string, style?: Phaser.Types.GameObjects.Text.TextStyle): Phaser.GameObjects.Text {
    style = style || FONT.GENERAL;
    let text: Phaser.GameObjects.Text = new Phaser.GameObjects.Text(this.currentScene, x, y, t, style);
    // NOTE: Shadow is not applied automatically. Possibly a bug?
    text.setShadow(style.shadow.offsetX, style.shadow.offsetY, style.shadow.color);
    text.setDepth(DEPTH.UI_TEXT);
    text.setOrigin(0.5, 0.5); // centered origin
    text.setScrollFactor(0, 0); // won't move when the camera scrolls
    this.currentScene.add.existing(text);
    this.group.add(text);
    return text;
  }
}