import { game } from '../game';
import { Manager } from './manager';
import { Globals } from '../common/globals';

export class TimeManager extends Manager {

  private timer: Phaser.Time.TimerEvent | null;

  init(scene: Phaser.Scene): void {
    super.init(scene);
    this.reset();
    this.timer = scene.time.addEvent({
      delay: 1000,
      callback: () => { this.onTimeEvent(); },
      loop: true,
    });
  }

  reset(): void {
    Globals.time = 0;
  }

  pause(): void {
    if (this.timer) {
      this.timer.paused = true;
    }
  }

  resume(): void {
    if (this.timer) {
      this.timer.paused = false;
    }
  }

  cleanup(): void {
    this.currentScene.time.removeAllEvents();
    this.timer.destroy();
    this.timer = null;
    super.cleanup();
  }

  private onTimeEvent(): void {
    Globals.increaseTime();
    game.panelManager.redraw();
  }
}