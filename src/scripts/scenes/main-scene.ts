import { game } from '../game';
import { Player } from '../objects/actors';
import { Globals } from '../common/globals';
import { SCENE } from '../common/constants';

export enum GameState {
  None,
  Init,
  Creating,
  Playing,
  WaitForRestart,
  Restarting,
}

export class MainScene extends Phaser.Scene {

  private gameState: GameState = GameState.None; // by default

  constructor() {
    super({ key: SCENE.MAIN })
  }

  create(): void {
    this.initManagers();
    this.createGameWorld();
    this.startGame();
  }

  update(time: number, delta: number): void {
    switch (this.gameState) {

      case GameState.Playing:
        let player: Player = game.playerManager.Player;
        if (player != null && !player.IsTerminated) {
          // Handle player input
          if (game.inputManager.IsDown) {
            player.move();
            if (game.textManager.IsHelpVisible) {
              game.textManager.setHelpTextVisible(false);
            }
          } else {
            player.rotate();
          }
          // Call player's update
          player.update(time, delta);
        }
        break;

      case GameState.WaitForRestart:
        if (game.inputManager.IsDown) {
          this.restartGame();
        }
        break;
    }
    // Updating managers, if needed
    game.inputManager.update(delta, time);
  }

  private initManagers(): void {
    this.setGameState(GameState.Init);
    console.log('Initializing managers');
    game.cameraManager.init(this);
    game.inputManager.init(this);
    game.itemManager.init(this);
    game.mapManager.init(this);
    game.panelManager.init(this);
    game.particleManager.init(this);
    game.playerManager.init(this);
    game.textManager.init(this);
    game.timeManager.init(this);
    Globals.reset();
  }

  private cleanup(): void {
    console.log('Cleanup managers');
    game.cameraManager.cleanup();
    game.inputManager.cleanup();
    game.itemManager.cleanup();
    game.mapManager.cleanup();
    game.panelManager.cleanup;
    game.particleManager.cleanup();
    game.playerManager.cleanup();
    game.textManager.cleanup();
    game.timeManager.cleanup();
  }

  private createGameWorld(): void {
    this.setGameState(GameState.Creating);
    console.log('Creating game world');
    // Order is important here!
    game.mapManager.createMap(Globals.map);
    game.playerManager.createPlayer(game.mapManager.CurrentMap, () => this.onPlayerDied(), () => this.onLevelCompleted());
    game.itemManager.createItems(game.mapManager.CurrentMap);
    game.particleManager.createParticles();
    game.panelManager.createPanel();
    game.textManager.createTexts();
    game.cameraManager.setupCameras(); // this should be the last
  }

  private waitForRestart(): void {
    game.inputManager.reset();
    this.setGameState(GameState.WaitForRestart);
  }

  private startGame(): void {
    game.panelManager.redraw();
    if (!Globals.helpTextShown) {  // display help text only once
      game.textManager.setHelpTextVisible(true);
      Globals.helpTextShown = true;
    }
    game.cameraManager.fadeIn(() => {
      console.log('Starting game');
      game.inputManager.reset();
      this.setGameState(GameState.Playing);
    });
  }

  private restartGame(): void {
    this.setGameState(GameState.Restarting);
    Globals.reset();
    // Restarting after camera faded out
    game.cameraManager.fadeOut(() => {
      // TODO: implement proper restarting here
      this.cleanup();
      this.scene.restart();
      console.log('Restarting game');
    });
  }

  private setGameState(gameState: GameState, delay?: number, callback?: Function): void {
    // Waits for a small amount of time before the next state
    if (delay) {
      setTimeout(() => {
        this.setGameState(GameState.WaitForRestart);
        if (callback) {
          callback();
        }
      }, delay)
    // Immediate
    } else {
      this.gameState = gameState;
    }
  }

  private onPlayerDied(): void {
    game.textManager.setGameOverTextVisible(true);
    this.waitForRestart();
  }

  private onLevelCompleted(): void {
    game.textManager.setLevelCompletedTextVisible(true);
    this.waitForRestart();
  }
}
