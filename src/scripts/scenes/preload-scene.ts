import { DIRECTORY, EXTENSION, MAP, SCENE, SPRITE, TILE_DATA, PARTICLE } from '../common/constants';

declare var WebFont: any;

export class PreloadScene extends Phaser.Scene {

  private fontLoaded: boolean = false;

  constructor() {
    super({ key: SCENE.PRELOAD })
  }

  preload(): void {
    this.load.on('progress', this.onLoadProgress, this);
    this.load.on('complete', this.onLoadComplete, this);
    this.loadSprites();
    this.loadParticles();
    this.loadTilemaps();
    this.loadScripts();
  }

  create(): void {
    this.createFonts();
  }

  update(): void {
    if (this.fontLoaded) {
      this.scene.start(SCENE.MAIN);
    }
  }

  private onLoadProgress(progress: number): void {
    // Not used at this time
    // console.log(`${Math.round(progress * 100)}%`);
  }

  private onLoadComplete(loader: any, completed: number, failed: number): void {
    console.log('Loading finished');
    console.log('Completed: ', completed);
    console.log('Failed: ', failed);
  }

  private loadScripts(): void {
    //this.load.script('webfont', 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js'); // load from googleapis
    this.load.script('webfont', DIRECTORY.JS + 'webfont/webfont.js');
  }

  private loadSprites(): void {
    Object.keys(SPRITE).forEach((key) => {
      let spriteId: string = SPRITE[key];
      this.load.image(spriteId, DIRECTORY.IMAGES.SPRITES + spriteId + EXTENSION.PNG);
    });
  }

  private loadParticles(): void {
    Object.keys(PARTICLE).forEach((key) => {
      let particleId: string = PARTICLE[key];
      this.load.image(particleId, DIRECTORY.IMAGES.PARTICLES + particleId + EXTENSION.PNG);
    });
  }

  private loadTilemaps(): void {
    this.load.image(TILE_DATA.IMAGE, DIRECTORY.IMAGES.TILESETS + TILE_DATA.IMAGE + EXTENSION.PNG);
    Object.keys(MAP).forEach((key) => {
      let mapId: string = MAP[key];
      this.load.tilemapTiledJSON(mapId, DIRECTORY.MAPS + mapId + EXTENSION.JSON);
    });
  }

  private createFonts(): void {
    WebFont.load({
      custom: {
        families: ['mikropix']
      },
      active: () => {
        this.fontLoaded = true;  // wait for the font to be loaed
      }
    });
  }
}
