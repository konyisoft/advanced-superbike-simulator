# Advanced Superbike Simulator

![Screenshot](src/assets/misc/og.jpg)

## About

**Advanced Superbike Simulator** is a simple one-button game in which the player navigates through a racetrack with a motorcycle, trying to avoid different obstacles. The bike continuously rotates and can be accelerated with the following controls:

- pressing *Space* or *Enter* keys,
- pressing the left mouse button,
- or touching the screen on mobile devices.

The game created with [Phaser 3](https://phaser.io/) and TypeScript as a progressive web application (PWA).

## Demo

A playable version of the game [can be found here.](https://superbike.konyisoft.eu)

## Install

To try out this project, first you will have [Node.js](https://nodejs.org/en/) and [Git](https://git-scm.com/) installed on your computer.

Download (clone) the files of the project to your local computer:

```
$ git clone git@gitlab.com:konyisoft/advanced-superbike-simulator.git
```

Navigate to the project directory and run the following command to install the required packages:

```
$ cd advanced-superbike-simulator/
$ npm install
```

Open the project in your default browser with this command:

```
$ npm start
```

To create the production build into the */dist* directory, type the following:

```
$ npm run build
```

## License

MIT

----

Krisztian Konya, Konyisoft, 2020  
[konyisoft.eu](https://konyisoft.eu/)